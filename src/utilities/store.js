import store from 'react-native-simple-store';

export const getStoreItem = (key) => {
  return store.get(key)
  .then(value => {
    return value;
  });
};

export const mapCurrentPrincipleToState = () => {
  store.get('currentPrinciple')
  .then(currentPrinciple => {
    this.setState({
      currentPrinciple,
      loading: false
    });
  })
  .catch(error => {
    console.error(error.message);
  });
}
