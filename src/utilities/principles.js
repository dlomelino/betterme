import store from '../store/store'
import sources from '../assets/sources'
// import { store as localStorage } from 'react-native-simple-store'
import moment from 'moment'

const aggregatedPrinciples = [].concat.apply([], sources.map(source => {
  return source.principles.map(principle => {
    return {
      id: principle.id,
      title: principle.title,
      concepts: principle.concepts,
      // have to break out 'source' properties rather than attach it as an object
      // in order to put them into the store for some reason
      sourceId: source.id,
      sourceTitle: source.title,
      sourceAuthor: source.author,
      sourceYear: source.year
    };
  });
}));

const attachSourcetoPrinciple = (principle) => {
  principle = aggregatedPrinciples.filter(aggregatedPrinciple => {

  })

  return principle
}

export const getRandomPrinciple = () => {
  var randomIndex = Math.floor(Math.random() * aggregatedPrinciples.length);

  return aggregatedPrinciples[randomIndex];
}

export const getSourceByPrinciple = (findPrinciple) => {
  return sources.map(source => {
    var match = source.principles.filter(principle => {
      return principle.title === findPrinciple;
    });

    if (match.length) {
      return {
        ...source,
        principle: match[0]
      };
    } else {
      return {}
    }
  })
  .filter(source => {
    return Object.keys(source).length > 0;
  })[0];
};

export const beginPrinciple = (principle) => {
  console.log('got yo principle', attachSourcetoPrinciple(principle))
}

// export const updateProgress = (principle) => {
//   localStorage.get('practicedPrinciples')
//   .then(principles => {
//   })
// }

export const workedOnDate = () => {
  return moment().format('YYYYMMDD')
}
