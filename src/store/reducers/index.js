import { combineReducers } from 'redux';
import principleReducer from './principleReducer';

export default combineReducers({
  principle: principleReducer
});
