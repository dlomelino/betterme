export default (state = {}, action) => {
  switch (action.type) {
    case 'SET_CURRENT_PRINCIPLE':
      return {
        ...state,
        currentPrinciple: action.payload
      };
    case 'SET_WORKED_ON_CURRENT_PRINCIPLE':
      return {
        ...state,
        workedOnCurrentPrinciple: action.payload
      };
    default:
      return state;
  }
};
