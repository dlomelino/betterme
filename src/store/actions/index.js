const setCurrentPrincipleAction = payload => {
  return {
    type: 'SET_CURRENT_PRINCIPLE',
    payload
  };
};

const setWorkedOnCurrentPrincipleAction = payload => {
  return {
    type: 'SET_WORKED_ON_CURRENT_PRINCIPLE',
    payload
  }
}

export const setCurrentPrinciple = (payload) => {
  return (dispatch, getState) => {
    dispatch(setCurrentPrincipleAction(payload));
  };
};

export const setWorkedOnCurrentPrinciple = (payload) => {
  return (dispatch, getState) => {
    dispatch(setWorkedOnCurrentPrincipleAction(payload));
  };
};
