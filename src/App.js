import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import Tabs from './config/router';

import store from './store/store';
import { globalBackground } from './assets/mixins';

console.disableYellowBox = true;

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.appContainer}>
          <View style={styles.topBar}>
          </View>
          <View style={styles.content}>
            <Tabs />
          </View>
        </View>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  appContainer: {
    flex: 1
  },
  topBar: {
   height: 20,
    backgroundColor: '#fff'
  },
  content: {
    ...globalBackground,
    flex: 1
  }
});
