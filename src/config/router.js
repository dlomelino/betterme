import React from 'react'
import { Button } from 'react-native'
import { TabNavigator, StackNavigator, NavigationActions } from 'react-navigation'
import { Icon } from 'react-native-elements'

import Home from '../components/home/Home'
import Principles from '../components/principles/Principles'
import Principle from '../components/principles/Principle'
import Progress from '../components/progress/Progress'
import Settings from '../components/settings/Settings'
import { beginPrinciple } from '../utilities/principles'

const HomeStackNav = StackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        headerTitle: 'Home'
      }
    },
    Principle: {
      screen: Principle,
      navigationOptions: {
        headerTitle: 'Principle'
      }
    }
  }
)

const PrinciplesStackNav = StackNavigator(
  {
    Principles: {
      screen: Principles,
      navigationOptions: {
        headerTitle: 'Principles'
      }
    },
    Principle: {
      screen: Principle,
      navigationOptions: ({ navigation }) => ({
        headerTitle: 'Principle',
        headerRight: <Button title="Begin" onPress={() => { beginPrinciple(navigation.state.params.principle) }} />
      })
    }
  }
)

const SettingsStackNav = StackNavigator(
  {
    Settings: {
      screen: Settings,
      navigationOptions: {
        headerTitle: 'Settings'
      }
    }
  }
)

const Tabs = TabNavigator(
  {
    Home: {
      screen: HomeStackNav,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({ tintColor }) => <Icon name="home" size={35} color={tintColor} />
      }
    },
    // v2.0 TODO
    // Progress: {
    //   screen: Progress,
    //   navigationOptions: {
    //     tabBarLabel: 'Progress',
    //     tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    //   }
    // },
    Principles: {
      screen: PrinciplesStackNav,
      navigationOptions: {
        tabBarLabel: 'Principles',
        tabBarIcon: ({ tintColor }) => <Icon name="list" size={35} color={tintColor} />
      }  
    }
    // Settings: {
    //   screen: SettingsStackNav,
    //   navigationOptions: {
    //     tabBarLabel: 'Settings',
    //     tabBarIcon: ({ tintColor }) => <Icon name="gear" type="font-awesome" size={35} color={tintColor} />
    //   }
    // }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarOnPress: ({ previousScene, scene, jumpToIndex }) => {
        // reset the stack navigator that we are coming from
        navigation.dispatch(NavigationActions.reset({
          key: previousScene.key,
          index: 0,
          actions: [NavigationActions.navigate({routeName: previousScene.routeName })]
        }))

        // move along...
        jumpToIndex(scene.index)
      }
    })
  }
)

export default Tabs
