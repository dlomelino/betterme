export const globalBackground = {
  backgroundColor: '#dc143c'
}

export const globalTitle = {
  fontSize: 24,
  fontWeight: 'bold',
  color: '#fff'
}

export const globalText = {
  color: '#fff'
}

export const listItem = {
  borderBottomWidth: 1,
}

export const listItemInner = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingTop: 5,
  paddingRight: 10,
  paddingBottom: 5,
  paddingLeft: 10,
  width: '100%',
  height: 40
}

export const listText = {
  fontSize: 20,
  fontWeight: 'bold',
  color: '#fff'
}
