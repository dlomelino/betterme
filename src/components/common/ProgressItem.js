import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ProgressBar from 'react-native-progress/Bar';

import { globalTitle, globalText } from '../../assets/mixins';

export default class ProgressItem extends Component {
  constructor() {
    super();
    this.progressBarProps = {
      width: null,
      height: 15,
      unfilledColor: '#fff',
      borderRadius: 0
    };
  }

  render() {
    return (
      <View style={styles.progressItem}>
        { this.props.item &&
        <Text style={styles.text}>{this.props.item.key}</Text>
        }
        <View>
          <ProgressBar
            progress={0.3}
            width={this.progressBarProps.width}
            height={this.progressBarProps.height}
            unfilledColor={this.progressBarProps.unfilledColor}
            borderRadius={this.progressBarProps.borderRadius}
            style={styles.progressBar}
          />
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  progressItem: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    marginTop: 5
  },
  text: {
    ...globalText,
    fontSize: 20,
    fontWeight: 'bold'
  },
  progressBar: {
    marginTop: 5
  }
});
