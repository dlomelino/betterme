import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { listItem, listItemInner, listText } from '../../assets/mixins'

export default class ListItem extends Component {
  render () {
    return (
      <View style={styles.listItem}>
        <TouchableOpacity>
          <View style={styles.listItemInner}>
            <Text style={styles.listText}>{this.props.label}</Text>
            {this.props.arrow && 
              <Text style={styles.listText}>></Text>
            }
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  listItem,
  listItemInner,
  listText
})
