import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

export default class CustomActivityIndicator extends Component {
  render() {
    return (
      <ActivityIndicator
        animating={this.props.animating}
        color="#fff"
        size="large"
      />
    );
  }
};
