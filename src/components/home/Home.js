import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Switch } from 'react-native';
import { connect } from 'react-redux';
import store from 'react-native-simple-store';

import CustomActivityIndicator from '../common/CustomActivityIndicator';
import PrincipleBox from '../principles/PrincipleBox';
// import ProgressItem from '../common/ProgressItem';
import { setCurrentPrinciple, setWorkedOnCurrentPrinciple } from '../../store/actions';
import { globalText, globalBackground } from '../../assets/mixins';
import { getRandomPrinciple, workedOnDate } from '../../utilities/principles';

class Home extends Component {
  constructor () {
    super();
    this.state = {
      isLoading: true,
      workedOnSwitchValue: false
    };
  }

  componentDidMount() {
    this.init();
  }

  init() {
    // get everything set when the app starts

    // get the current principle that the user is working on
    store.get('currentPrinciple')
    .then(currentPrinciple => {
      if (currentPrinciple) {
        // set the current principle in the app state
        this.props.setCurrentPrinciple(currentPrinciple);

        // check whether the initially loaded current principle
        // has been marked as "worked on" for today
        this.hasBeenWorkedOnToday(currentPrinciple);
      }
    })
    .then(() => {
      // turn off loading indicator
      this.setState({
        isLoading: false
      });
    })
    .catch(error => {
      console.error(error.message);
    });
  }

  onPressRandom = () => {
    var principle = getRandomPrinciple();

    // update app state with the newly generated principle
    this.props.setCurrentPrinciple(principle);
    // save newly generated principle to persistent storage
    store.save('currentPrinciple', principle);

    // check whether this principle has already been marked as "worked on" today
    this.hasBeenWorkedOnToday(principle);
  }

  onPressPrinciple = () => {
    // when the user presses the current principle box,
    // take them to the principle detail screen
    var principle = {
      id: this.props.currentPrinciple.id,
      title: this.props.currentPrinciple.title,
      concepts: this.props.currentPrinciple.concepts
    };

    this.props.navigation.navigate('Principle', { principle });
  }

  onPressWorkedOn = (hasWorkedOn) => {
    var today = workedOnDate();
    var objWorkedOn = {
      sourceId: this.props.currentPrinciple.sourceId,
      principleId: this.props.currentPrinciple.id
    };

    // tell the rest of the app whether the user has worked on this principle today
    this.props.setWorkedOnCurrentPrinciple(hasWorkedOn);

    if (hasWorkedOn) {
      // log that the user has worked on this principle today
      store.push(today, objWorkedOn);
    } else {
      // the user is unmarking this principle as "worked on"; remove it from storage
      var workedOnToday = null;

      store.get(today)
      .then(result => {
        // store this outside of the promise chain for access in subsequent "then" blocks
        workedOnToday = result;
        
        // remove the entire day
        return store.delete(today);
      })
      .then(() => {
        var workedOnTodayLength = workedOnToday.length;

        if (workedOnTodayLength > 1) {
          // if this was not the only principle worked on today, re-add the others
          var newWorkedOnToday = workedOnToday.filter(w => {
            return w.sourceId !== objWorkedOn.sourceId || (w.sourceId === objWorkedOn.sourceId && w.principleId !== objWorkedOn.principleId);
          });

          store.save  (today, newWorkedOnToday);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
    }
  }

  hasBeenWorkedOnToday = (principle) => {
    var today = workedOnDate();
    var objWorkedOn = {
      sourceId: principle.sourceId,
      principleId: principle.id
    };

    store.get(today)
    .then(workedOnToday => {
      if (workedOnToday && workedOnToday.length > 0) {
        var workedOnToday = workedOnToday.filter(w => {
          return w.sourceId === objWorkedOn.sourceId && w.principleId === objWorkedOn.principleId;
        });

        if (workedOnToday.length > 0) {
          this.props.setWorkedOnCurrentPrinciple(true);
        } else {
          this.props.setWorkedOnCurrentPrinciple(false);
        }
      }
    })
    .catch(error => {
      console.error(error.message);
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loadingContainer}>
          <CustomActivityIndicator animating={this.state.isLoading} />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          { !this.props.currentPrinciple &&
            // The app is being used for the first time or the user has never chosen a principle
            <View>
              <Text style={[styles.text, styles.paragraph]}>Welcome to Better Me!, the app for incorporating self-improvement principles into your life from world-renowned sources!</Text>
              <Text style={[styles.text, styles.paragraph]}>To get started, press the button below to begin practicing a principle at random or use the bottom navigation to go to the Principles section and select one for yourself.</Text>
              <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={this.onPressRandom} style={styles.button}>
                  <Text style={styles.buttonText}>Choose Random Principle</Text>
                </TouchableOpacity>
              </View>
            </View>
          }
          { this.props.currentPrinciple &&
            // This is a returning user that has chosen a principle to work on
            <View style={styles.currentPrincipleContainer}>
              <Text style={styles.text}>You are currently working on...</Text>
              <TouchableOpacity onPress={this.onPressPrinciple}>
                <PrincipleBox principle={this.props.currentPrinciple} style={styles.principleBox} />
              </TouchableOpacity>
              {/* <Text style={styles.text}>You have spent <Text style={styles.numDays}>4</Text> days on this</Text> */}
              {/* <ProgressItem /> */}
              {/* <TouchableOpacity onPress={this.onPressWorkedOn.bind(this, !this.props.workedOnCurrentPrinciple)} style={this.props.workedOnCurrentPrinciple ? [styles.button, styles.buttonToggleRed] : [styles.button, styles.buttonToggleGreen]}>
                <Text style={styles.buttonText}>
                  { this.props.workedOnCurrentPrinciple
                    ? <Text style={styles.buttonText}>I have not worked on this today</Text>
                    : <Text style={styles.buttonText}>I worked on this today</Text>
                  }
                </Text>
              </TouchableOpacity> */}
              <View style={styles.row}>
                <Text style={styles.boldWhite}>I worked on this today</Text>
                <Switch value={this.props.workedOnCurrentPrinciple} onValueChange={(value) => { this.onPressWorkedOn(value) }} />
              </View>
              <View style={styles.bottom}>
                <TouchableOpacity onPress={this.onPressRandom} style={styles.button}>
                  <Text style={styles.buttonText}>New Random Principle</Text>
                </TouchableOpacity>
              </View>
            </View>
          }
        </View>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    currentPrinciple: state.principle.currentPrinciple,
    workedOnCurrentPrinciple: state.principle.workedOnCurrentPrinciple
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentPrinciple: (payload) => dispatch(setCurrentPrinciple(payload)),
    setWorkedOnCurrentPrinciple: (payload) => dispatch(setWorkedOnCurrentPrinciple(payload))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    ...globalBackground,
    flex: 1,
    padding: 20,
  },
  currentPrincipleContainer: {
    flex: 1
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  text: {
    ...globalText,
    fontSize: 20,
    fontWeight: 'bold'
  },
  principleBox: {
    marginTop: 5,
    marginBottom: 10,
    borderWidth: 2
  },
  paragraph: {
    marginBottom: 20
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  boldWhite: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    padding: 5
  },
  buttonContainer: {
    marginTop: 10
  },
  button: {
    height: 50,
    borderWidth: 2,
    borderColor: '#000',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  numDays: {
    fontSize: 24
  }
});
