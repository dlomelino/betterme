import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import store from 'react-native-simple-store';
import ProgressBar from 'react-native-progress/Bar';

import CustomActivityIndicator from '../common/CustomActivityIndicator';
import ProgressItem from '../common/ProgressItem';
import { globalTitle, globalText } from '../../assets/mixins';
import sources from '../../assets/sources';
import { mapCurrentPrincipleToState } from '../../utilities/store';

class Progress extends Component {
  componentDidMount() {
    // Get all of the principles that the user has worked on
  }

  onPress = () => {
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Current Principle</Text>
        <View style={styles.progressContainer}>
          { this.props.currentPrinciple
            ? 
            // the user has chosen a principle
            <View>
              <ProgressItem item={{item: {key: this.props.currentPrinciple.title }}} />
              <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={this.onPress} style={styles.button}>
                  <Text style={styles.buttonText}>I worked on this today</Text>
                </TouchableOpacity>
              </View>
            </View>
            :
            // the user has navigated to this Progress screen
            // without having ever chosen a principle
            <View style={styles.noPrincipleContainer}>
              <Text style={styles.noPrinciple}>No principle selected</Text>
            </View>
          }
        </View>
        <Text style={styles.header}>All Principles</Text>
        <View style={styles.progressContainer}>
          {/* <FlatList
            data={principlesWithKeys}
            renderItem={this.renderProgressItem}
            ItemSeparatorComponent={() => <View style={styles.listSeparator} />}
            ListHeaderComponent={this.principlesHeader}
            style={styles.list}
          /> */}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentPrinciple: state.principle.currentPrinciple
  };
};

export default connect(mapStateToProps)(Progress);

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1
  },
  header: {
    ...globalTitle,
    margin: 10
  },
  progressContainer: {
    marginBottom: 20
  },
  text: {
    ...globalText,
    fontSize: 20,
    fontWeight: 'bold'
  },
  listSeparator: {
    height: 2,
    backgroundColor: '#fff'
  },
  list: {
  },
  buttonContainer: {
    marginTop: 10,
    alignItems: 'center'
  },
  button: {
    height: 50,
    width: '80%',
    borderWidth: 2,
    borderColor: '#000',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  noPrincipleContainer: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    marginTop: 5
  },
  noPrinciple: {
    fontSize: 18,
    fontWeight: 'bold'
  }
});
