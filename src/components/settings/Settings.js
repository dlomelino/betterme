import React, { Component } from 'react'
import { View, Text, FlatList, StyleSheet, Switch, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import store from 'react-native-simple-store'

import CustomActivityIndicator from '../common/CustomActivityIndicator'
import { globalTitle, globalText, globalBackground } from '../../assets/mixins'
import ListItem from '../common/ListItem'
import sources from '../../assets/sources'

class Settings extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.sectionContainer}>
          <View style={styles.sectionHeader}>
            <Text style={styles.sectionHeaderText}>Sources</Text>
          </View>
          {sources.map(source => {
            return (
              <View style={styles.sectionBody}>
                <View style={styles.row}>
                  <View style={styles.leftCol}>
                    <Text style={styles.rowText}>{source.title}</Text>
                  </View>
                  <View style={styles.rightCol}>
                    <Switch value={source.enabled} onValueChange={(value) => { return true }} />
                  </View>
                </View>
              </View>  
            )
          })}
        </View>
      </View>
    )
  }
}

export default connect()(Settings)

const styles = StyleSheet.create({
  container: {
    ...globalBackground,
    flex: 1,
    padding: 10
  },
  sectionContainer: {
    borderWidth: 1,
    backgroundColor: '#fff',
    color: '#000'
  },
  sectionHeader: {
    ...globalTitle,
    height: 50,
    borderBottomWidth: 1
  },
  sectionHeaderText: {
    ...globalTitle,
    margin: 10,
    color: '#000'
  },
  sectionBody: {
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    padding: 5,
    alignItems: 'center'
  },
  leftCol: {
    flex: 1
  },
  rowText: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 5
  },
  rightCol: {
  }
})
