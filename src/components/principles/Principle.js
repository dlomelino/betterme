import React, { Component } from 'react'
import { Button, FlatList, StyleSheet, Text, View } from 'react-native'
import store from 'react-native-simple-store'

import PrincipleBox from './PrincipleBox'
import { globalBackground } from '../../assets/mixins'
import ListItem from '../common/ListItem'
import { beginPrinciple } from '../../utilities/principles'

export default class Principle extends Component {
  render() {
    var principle = this.props.navigation.state.params.principle;

    return (
      <View style={styles.container}>
        <PrincipleBox principle={principle} style={styles.principleBox} />
        <View style={styles.conceptsContainer}>
          <ListItem label="Concepts" arrow="true" />
          <ListItem label="Examples" arrow="true" />
          <ListItem label="Quotes" arrow="true" />
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    ...globalBackground,
    flex: 1
  },
  principleBox: {
    borderTopWidth: 2,
    borderBottomWidth: 2
  },
  conceptsContainer: {
    flex: 1
  }
})
