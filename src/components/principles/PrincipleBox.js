import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { globalText } from '../../assets/mixins';

export default class PrincipleBox extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={[styles.text, styles.principleText]}>{this.props.principle.title}</Text>
        { this.props.principle.sourceTitle &&
          <Text style={styles.principleSource}>{this.props.principle.sourceTitle}</Text>
        }
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: '#fff'
  },
  text: {
    ...globalText,
    fontSize: 20,
    fontWeight: 'bold'
  },
  principleText: {
    color: '#000',
    fontSize: 24
  },
  principleSource: {
    marginTop: 20,
    fontStyle: 'italic'
  }
});
