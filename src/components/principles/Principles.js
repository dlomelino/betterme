import React, { Component } from 'react';
import { Text, View, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import store from 'react-native-simple-store';

import { globalTitle, globalText } from '../../assets/mixins';
import sources from '../../assets/sources';

export default class Principles extends Component {
  onPressPrinciple = (principle) => {
    // when the user presses a principle, take
    // them to the principle detail screen
    this.props.navigation.navigate('Principle', { principle });
  }

  renderPrinciple = ({item}) => {
    return (
      <TouchableOpacity onPress={() => this.onPressPrinciple(item)}>
        <Text style={styles.listItem}>{item.title}</Text>
      </TouchableOpacity>
    );
  }

  keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.bookTitle}>{sources[0].title}</Text>
          <Text style={styles.bookDetails}>by {sources[0].author}; published {sources[0].year}</Text>
        </View>
        <FlatList
          data={sources[0].principles}
          renderItem={this.renderPrinciple}
          keyExtractor={this.keyExtractor}
          ItemSeparatorComponent={() => <View style={styles.listSeparator} />}
          style={styles.list}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#dc143c'
  },
  list: {
    flex: 1,
  },
  listItem: {
    ...globalText,
    padding: 10,
    fontSize: 18
  },
  listSeparator: {
    height: 2,
    backgroundColor: '#fff'
  },
  headerContainer: {
    padding: 10,
    borderBottomWidth: 2,
    borderColor: '#fff'
  },
  bookTitle: {
    ...globalTitle
  },
  bookDetails: {
    ...globalText,
    marginTop: 5,
    fontStyle: 'italic'
  }
});
